Deployment Instructions

$ ionic serve
$ ionic build
$ vi www/index.html 
	Add: <base href="/aspenwarev2/" />
$ ./build.sh

Payload JSON Objects:

dateFormat, vendor will pass us a format for how they like the date to be displayed and collected for their clients

POS = used to determine which POS system this client is using.  If POS = “SW” then we have more work to perform after the waiver is submitted.   if POS = “RTP” we are done  See AD-42

posWaiverId is their POS’s internal Waiver ID, it gets mapped to an element in SignedDoc (use to be waiverID
posGuestID, same as above, but for guest.

/?g=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ3YWl2ZXJzIjpbeyJ3YWl2ZXJJZCI6ImFIUjBjSE02THk5a1pYWXVNWEpwYzJzdWFXNW1ieTlzWlhOemIyMXRaWFJ6YkdGaVlYQnBmRE5pWVRWa1l6Um1NelppWmpSa1lXVTRZemM0WWpJNFlXTTJNR1prTkRneGZERT0iLCJmb3JtSWQiOiIzMDAwMDAwIiwiZ3Vlc3RzIjpbeyJndWVzdElkIjoiOTc1OTkxMDAwMDAwIiwiZmlyc3ROYW1lIjoiVGVzdCIsImxhc3ROYW1lIjoiUGFwYSIsImRvYiI6IjE5NzAwMTAxIn1dfV19.IxEhbTRdN6DO4u62RiBHDzFafKEkJ6OqJpB0AMM92v0

{
  "waivers": [
    {
      "waiverId":"aHR0cHM6Ly9kZXYuMXJpc2suaW5mby9sZXNzb21tZXRzbGFiYXBpfDNiYTVkYzRmMzZiZjRkYWU4Yzc4YjI4YWM2MGZkNDgxfDE=",
      "formId": "3000000",
      "guests": [
        {
          "guestId": "975991000000",
          "firstName": "Test",
          "lastName": "Papa",
          "dob": "19700101"
        }
      ]
    }
  ]
}




///les prod waiver

/?g=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ3YWl2ZXJzIjpbeyJ3YWl2ZXJJZCI6ImFIUjBjSE02THk5dGVTNHhjbWx6YXk1dVpYUXZiR1Z6YzI5dGJXVjBjMkZ3YVh3M1l6azVOMlV4WTJNNU1tVTBaRGd5T1dGbFlqZGlNMlF3WkRCaE1ETmhaSHd6IiwiZm9ybUlkIjoiMzAwMDAwMCIsImd1ZXN0cyI6W3siZ3Vlc3RJZCI6Ijk3NTk5MTAwMDAwMCIsImZpcnN0TmFtZSI6IlRlc3QiLCJsYXN0TmFtZSI6IlBhcGEiLCJkb2IiOiIxOTcwMDEwMSJ9XX1dfQ.cnu-R8qiFlCnVd5MMqcdUn-Pc3hbnv4BnUpFMR87TzQ

{
  "waivers": [
    {
      "waiverId":"aHR0cHM6Ly9teS4xcmlzay5uZXQvbGVzc29tbWV0c2FwaXw3Yzk5N2UxY2M5MmU0ZDgyOWFlYjdiM2QwZDBhMDNhZHwz",
      "formId": "3000000",
      "guests": [
        {
          "guestId": "975991000000",
          "firstName": "Test",
          "lastName": "Papa",
          "dob": "19700101"
        }
      ]
    }
  ]
}



// LES does not use new payload structure 
{
	"dateFormat": "mm/dd/yyyy",
	"waivers": [
		{
			"waiverId": "aHR0cHM6Ly9kZXYuMXJpc2suaW5mby90ZXN0YXBpfDcyNmMwYjVhMTcwMDQxMmRiZmFiNjUzNjQ3MDc4ODQ5fDE=",
			"posWaiverId": "40015",
			"pos": "RTP",
			"guests": [
				{
					"posGuestID": "1007150",
					"firstName": "Joe",
					"lastName": "Smith",
					"dob": "19900101",
					"email": "mymail@mail.com",
					"phone": "3014441234",
					"address": {
						"street1": "1 Main St",
						"street2": null,
						"city": "Frederick",
						"country": "US",
						"state": "MD",
						"postal": "21720"
					}
				}
			]
		}
	]
} 


/?g=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRlRm9ybWF0IjoibW0vZGQveXl5eSIsIndhaXZlcnMiOlt7IndhaXZlcklkIjoiYUhSMGNITTZMeTlrWlhZdU1YSnBjMnN1YVc1bWJ5OTBaWE4wWVhCcGZEY3lObU13WWpWaE1UY3dNRFF4TW1SaVptRmlOalV6TmpRM01EYzRPRFE1ZkRFPSIsInBvc1dhaXZlcklkIjoiNDAwMTUiLCJwb3MiOiJSVFAiLCJndWVzdHMiOlt7InBvc0d1ZXN0SUQiOiIxMDA3MTUwIiwiZmlyc3ROYW1lIjoiSm9lIiwibGFzdE5hbWUiOiJTbWl0aCIsImRvYiI6IjE5OTAwMTAxIiwiZW1haWwiOiJteW1haWxAbWFpbC5jb20iLCJwaG9uZSI6IjMwMTQ0NDEyMzQiLCJhZGRyZXNzIjp7InN0cmVldDEiOiIxIE1haW4gU3QiLCJzdHJlZXQyIjpudWxsLCJjaXR5IjoiRnJlZGVyaWNrIiwiY291bnRyeSI6IlVTIiwic3RhdGUiOiJNRCIsInBvc3RhbCI6IjIxNzIwIn19XX1dfQ.OVrjP6YQ19IFk6lS9-IhJps4fy6fq1Vb_2smH9pkWbk



BUILD Instructions

ionic serve
ionic build
cd www
edit index.html and change href to:   <base href="/aspenwarev2/" />
cd ..
./build.sh to deploy to test


ionic build -- --base-href /aspenwarev2/