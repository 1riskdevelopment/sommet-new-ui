import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class DataService {
	// tslint:disable-next-line:indent
	// tslint:disable-next-line:indent
	constructor(private http: HttpClient) { }

	getLogo(waiverData) {
		const url = waiverData.apiUrl + '/api/v1/system/ui/logo/80';
		const params = new HttpParams().set('apiKey', waiverData.apiKey);
		return this.http.get(url, { params: params });
	}

	getPreferences(waiverData) {
		const url = waiverData.apiUrl + '/api/v1/siriusware/getsoap';
		const params = new HttpParams().set('apiKey', waiverData.apiKey);
		return this.http.get(url, { params: params });

	}

	getWaiver(waiverData) {
		const url = waiverData.apiUrl + '/api/v1/rsg/document/' + waiverData.waiverId;
		const params = new HttpParams().set('apiKey', waiverData.apiKey);
		return this.http.get(url, { params: params });
	}

	submitWaiver(waiverData, signedWaiver) {
		const url = waiverData.apiUrl + '/api/v1/rsg/signed/update/' + waiverData.apiKey;
		// const params = new HttpParams().set('apiKey', waiverData.apiKey)
		return this.http.post(url, signedWaiver, {});
	}

	backupSave(waiver, apiName, options) {
		this.http.post('https://staging.1risk.info/wb/waivers', { api: apiName, data: waiver }, options)
			.subscribe(data => {
			});
	}
	acceptSWLiability(swURL, values) {
		const split = swURL.split('/', 1);
		const splitDomainPort = split[0].split(':');
		const domain = splitDomainPort[0];
		const port = splitDomainPort[1];
		const index = swURL.indexOf('/');
		const service = swURL.substr(index);

		const payload = {
			domain: domain,
			port: port,
			service: service,
			formNo: values.formNo,
			guestNo: values.guestNo
		};
		// console.log(service)
		const url = 'https://rbw93kszxl.execute-api.us-east-1.amazonaws.com/dev' + '/swAcceptLiability';
		// console.log(payload);
		return this.http.post(url, payload, {});
	}

}
