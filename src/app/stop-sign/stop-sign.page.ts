import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { TranslateConfigService } from '../translate-config.service';


@Component({
  selector: 'app-stop-sign',
  templateUrl: './stop-sign.page.html',
  styleUrls: ['./stop-sign.page.scss'],
})
export class StopSignPage {
  @Input() msg: string = '';
  @Input() title: string = '';

  constructor(private router: Router, private modalCtrl: ModalController, private translate: TranslateConfigService) {

  }

  ionViewWillEnter() {
    if ((this.msg == null) || (this.msg === '')) {
      this.msg = 'Please read this waiver carefully!';
    }
    if ((this.title == null) || (this.title === '')) {
      this.msg = 'Please read this waiver carefully!';
    }
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }
}