import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StopSignPageRoutingModule } from './stop-sign-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { StopSignPage } from './stop-sign.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StopSignPageRoutingModule,
  ],
  declarations: [StopSignPage],
  entryComponents: [StopSignPage]
})
export class StopSignPageModule {}
