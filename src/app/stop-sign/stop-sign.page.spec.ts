import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StopSignPage } from './stop-sign.page';

describe('StopSignPage', () => {
  let component: StopSignPage;
  let fixture: ComponentFixture<StopSignPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopSignPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StopSignPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
