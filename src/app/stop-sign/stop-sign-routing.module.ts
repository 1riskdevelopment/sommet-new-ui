import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StopSignPage } from './stop-sign.page';

const routes: Routes = [
  {
    path: '',
    component: StopSignPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StopSignPageRoutingModule {}
