import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class EmailService {
	//URL = 'https://essegf9yil.execute-api.us-east-1.amazonaws.com/dev/';
  devURL = 'https://mail.testing.1riskint.com/';
  prodURL = 'https://mail.1riskint.com/';
	//URL = 'http://localhost:4000/';

  constructor(public httpClient: HttpClient) {
 		console.log('Email Service Started');
	}

  sendEmail(payload) {
    let url = this.devURL;

    if(window.location.hostname.toLowerCase().includes("rsg.")){
      url = this.prodURL;
    }
    this.httpClient.post(url + 'sendwaiver', payload)
      .subscribe(data => {}, error => {});
  }

  sendBarcode(payload) {
    let url = this.devURL;

    if(window.location.hostname.toLowerCase().includes("rsg.")){
      url = this.prodURL;
    }
		this.httpClient.post(url + 'sendbarcode', payload)
			.subscribe(data => {}, error => {});
	}
}
