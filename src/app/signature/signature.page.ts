import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit, AfterViewChecked } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { SignaturePad } from 'angular2-signaturepad';
import { TranslateConfigService } from '../translate-config.service';

@Component({
	selector: 'app-signature',
	templateUrl: './signature.page.html',
	styleUrls: ['./signature.page.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class SignaturePage implements OnInit, AfterViewInit {
	@ViewChild(SignaturePad, { static: true }) signaturePad: SignaturePad;
	@ViewChild('signatureContainer') public signatureContainer;

	private signaturePadOptions  = {
		minWidth: 2,
		canvasWidth: 572,
		canvasHeight: 230
	};

	param = null;
	signatureData: any;
	isEmpty = true;
	title = 'Signature';
	padSidePadding = 14;

	constructor(private navParams: NavParams,
				private modalCtrl: ModalController, 
				private translate: TranslateConfigService) { }

	ngOnInit() {
		// breakpoint for "full width modal"
		this.param = {title: this.title};
		if(window.innerWidth < 768){
			this.signaturePadOptions.canvasWidth = window.innerWidth - this.padSidePadding * 2;
		}
	}

	// tslint:disable-next-line:use-lifecycle-interface
	ngAfterViewInit() {
		/* console.log('signature pad ngAfter', this.signaturePad);
		console.log(this.signaturePad.elementRef.nativeElement.parentElement.clientHeight)
		console.log(this.signaturePad.elementRef.nativeElement.parentElement.clientWidth)
		console.log(document.getElementsByTagName('app-signature'));
		*/
		if (this.signaturePad) {
			this.signaturePad.clear();
			this.signatureData = this.navParams.get('sig');
			this.title = this.navParams.get('title');
			if (this.signatureData) {
				this.isEmpty = false;
				this.signaturePad.fromDataURL(this.signatureData);
			}
		}
	}

	resizePad() {
		this.isEmpty = true;
		const that = this;
    this.signaturePad.set('canvasWidth', this.signatureContainer.el.offsetWidth - this.padSidePadding * 2);
	}

	drawComplete() {
		// will be notified of szimek/signature_pad's onEnd event
		console.log(this.signaturePad.toDataURL());
	}

	drawStart() {
		// will be notified of szimek/signature_pad's onBegin event
		this.isEmpty = false;
	}

	drawClear() {
		this.isEmpty = true;
		this.signaturePad.clear();
	}

	drawCancel() {
		this.modalCtrl.dismiss({ result: 'CANCEL' });
	}

	drawSave() {
		if (!this.isEmpty) {
			this.modalCtrl.dismiss({ result: 'UPDATED', sig: this.signaturePad.toDataURL() });
		}
	}
}
