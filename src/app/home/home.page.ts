import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, LoadingController, IonTabBar, IonContent, IonTabButton } from '@ionic/angular';
import { DataService } from '../data.service';
import { StatusTabsComponent } from './status-tabs/status-tabs.component';
import { EmailService } from '../email.service';
import { HttpClient } from '@angular/common/http';

import * as moment from 'moment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { SignaturePage } from '../signature/signature.page';
import { TranslateConfigService } from '../translate-config.service';
import { StopSignPage } from '../stop-sign/stop-sign.page'

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

	@ViewChild(IonContent, { static: false }) content: IonContent;
	@ViewChild('statusTabs') statusTabs: StatusTabsComponent;
	public test: string;
	payload = null;
	toBeSigned = [] as any;
	selectedWaiver = null as any;
	currentWaiver = null as any;
	currentGuest = null as any;
	pos = 'SW';   //Les is SW Only
	posWaiverId = null;
	agreeToTerms;
	logo = null as any;
	swURL = null as any;
	hasMultiple = false;
	formattedDate = '';
	hasInlineInitials = false;
	sectionedWaiver = [];
	signatureTitle = "Signature";
	initialsTitle = "Initials";
	stopTitle = "Stop";
	signedWaiver = {} as any;
	language = "en";

	dateTimeFormat = 'DD/MM/YYYY hh:mm A';
	dateFormat = 'DD/MM/YYYY';
	

	statusTabConfig = [
		{ tabName: 'Initial', sectionId: '#waiver-card' },
		{ tabName: 'Consent', sectionId: '#consent-card' },
		{ tabName: 'Sign', sectionId: '#sign-card' },
		{ tabName: 'Submit', sectionId: '#submit-card' }
	];

	constructor(
		private router: Router,
		private dataService: DataService,
		public modalController: ModalController,
		private loadingCtrl: LoadingController,
		private translateConfigService: TranslateConfigService,
		private emailService: EmailService,
		private http: HttpClient,
		private modalCtrl: ModalController,
	) {
	}
	ngOnInit() {
		//this.translateConfigService.setLanguage('en');
		this.translateConfigService.setLanguage(this.translateConfigService.getDefaultLanguage());
		this.language = this.translateConfigService.getDefaultLanguage().toLowerCase();
		
		// console.log(this.translateConfigService.getDefaultLanguage());
		if (window.navigator.language.toLowerCase() === 'en' || window.navigator.language.toLowerCase() === 'en-us') {
			this.dateTimeFormat = 'MM/DD/YYYY hh:mm A';
			this.dateFormat = 'MM/DD/YYYY';
		}else{
			this.dateTimeFormat = 'DD/MM/YYYY hh:mm A';
			this.dateFormat = 'DD/MM/YYYY';
		}

		const url = location.search;
		if (url.length > 0) {
			this.payload = url.substr(3);
			this.decodeURL();
			this.setStatusTranslation();
			this.translateConfigService.translate.get('HOME.initial').subscribe((res: string) => {
				this.initialsTitle = res;
			});
			this.translateConfigService.translate.get('HOME.sign').subscribe((res: string) => {
				this.signatureTitle = res;
			});
			this.translateConfigService.translate.get('HOME.stop').subscribe((res: string) => {
				this.stopTitle = res;
			});
		} else {
			this.router.navigate(['/error']);
		}
	}

	setStatusTranslation(){
		this.translateConfigService.translate.get('HOME.initial').subscribe((res: string) => {
			this.statusTabConfig[0] = { tabName: res, sectionId: '#waiver-card' }
		});
		this.translateConfigService.translate.get('HOME.consent').subscribe((res: string) => {
			this.statusTabConfig[1] = { tabName: res, sectionId: '#consent-card' }
		});
		this.translateConfigService.translate.get('HOME.sign').subscribe((res: string) => {
			this.statusTabConfig[2] = { tabName: res, sectionId: '#sign-card' }
		});
		this.translateConfigService.translate.get('HOME.submit').subscribe((res: string) => {
			this.statusTabConfig[3] = { tabName: res, sectionId: '#submit-card' }
		});
	}

	async showStop(message) {
			let modal = await this.modalCtrl.create({ component: StopSignPage, componentProps: { msg: message, title: this.stopTitle }, backdropDismiss: false });
			modal.onDidDismiss().then(data => {
				this.signedWaiver.sawStop = true;
			});
			modal.present();
	}

	decodeURL() {
		const helper = new JwtHelperService();
		this.toBeSigned = helper.decodeToken(this.payload);
		if (this.toBeSigned) {
			console.log('payload-initial', JSON.parse(JSON.stringify(this.toBeSigned)));
			console.log('payload', this.toBeSigned);
			for (let i = 0; i < this.toBeSigned.waivers.length; i++) {
				if (i > 0) {
					this.hasMultiple = true;
				}
				const decodedStr = atob(this.toBeSigned.waivers[i].waiverId);
				const parts = decodedStr.split('|');
				const paramMap = { index_id: parts[2], url: parts[0], key: parts[1] };
				this.toBeSigned.waivers[i].config = {
					apiUrl: paramMap.url,
					apiKey: paramMap.key,
					waiverId: paramMap.index_id,
					staging: false,
					apiName: paramMap.url.split('/')[3]
				};

				for (let y = 0; y < this.toBeSigned.waivers[i].guests.length; y++) {
					if (y > 0) {
						this.hasMultiple = true;
					}

					this.toBeSigned.waivers[i].guests[y].age = this.calcAge(this.toBeSigned.waivers[i].guests[y].dob);
					this.toBeSigned.waivers[i].guests[y].initialed = false;
					this.toBeSigned.waivers[i].guests[y].showInitialed = true;
					this.toBeSigned.waivers[i].guests[y].consent = false;
					this.toBeSigned.waivers[i].guests[y].showConsent = false;
					this.toBeSigned.waivers[i].guests[y].signed = false;
					this.toBeSigned.waivers[i].guests[y].showSigned = false;
					this.toBeSigned.waivers[i].guests[y].submitted = false;
					this.toBeSigned.waivers[i].guests[y].showSubmit = false;
					this.toBeSigned.waivers[i].guests[y].showGuardian = false;
				}
				this.getWaiver(this.toBeSigned.waivers[0].config, 0);
			}
			this.posWaiverId = this.toBeSigned.waivers[0].formId;
			this.getLogo(this.toBeSigned.waivers[0].config);
			this.getPreferences(this.toBeSigned.waivers[0].config);
			if(this.toBeSigned.language){
				this.translateConfigService.setLanguage(this.toBeSigned.language);
				this.language = this.toBeSigned.language.toLowerCase();
			}
			if (this.language.toLowerCase() === 'en' || this.language.toLowerCase() === 'en-us') {
				
				this.dateTimeFormat = 'MM/DD/YYYY hh:mm A';
				this.dateFormat = 'MM/DD/YYYY';
			}else{
				this.dateTimeFormat = 'DD/MM/YYYY hh:mm A';
				this.dateFormat = 'DD/MM/YYYY';
			}
			this.currentGuest = this.toBeSigned.waivers[0].guests[0];
			this.toBeSigned.waivers[0].guests[0].selected = true;
			this.selectedWaiver = this.toBeSigned.waivers[0];
			this.currentGuest.inlineInitials = {};
			if (this.toBeSigned.displayFormat) {
				this.dateFormat = this.toBeSigned.displayFormat.toUpperCase();
			}
			const dateString = this.currentGuest.dob;
			const guestDob: moment.Moment = moment(dateString);
			this.formattedDate = guestDob.format(this.dateFormat);
			// console.log('Selected Waiver: ', this.selectedWaiver);
		} else {
			this.router.navigate(['/error']);
		}
	}

	calcAge(gdob) {
		const today = new Date();
		const y = gdob.substr(0, 4);
		const mon = gdob.substr(4, 2);
		const d = gdob.substr(6, 2);
		const fdate = mon + '/' + d + '/' + y;

		const dob = new Date(fdate);
		// console.log(fdate, y, mon, d);

		let age = today.getFullYear() - dob.getFullYear();
		const m = today.getMonth() - dob.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < dob.getDate())) {
			age--;
		}
		return age;
	}

	getLogo(waiverData) {
		this.dataService.getLogo(waiverData)
			.subscribe((data: any) => {
				this.logo = 'data:image/png;base64,' + data.data.logo;
			});
	}

	getPreferences(waiverData) {
		this.dataService.getPreferences(waiverData)
			.subscribe((data: any) => {
				// console.log('Preferences: ', data)
				this.swURL = data.data;
			});
	}

	getWaiver(waiverData, index) {
		this.dataService.getWaiver(waiverData)
			.subscribe((data: any) => {
				if (index === 0) {
					this.currentWaiver = data.data;
				}
				this.toBeSigned.waivers[index].currentWaiver = data.data;
				// tslint:disable-next-line:prefer-for-of
				for (let y = 0; y < this.toBeSigned.waivers[index].guests.length; y++) {
					this.toBeSigned.waivers[index].guests[y].isMinor = false;

					if (this.toBeSigned.waivers[index].currentWaiver.minAdultAge) {
						// console.log('MIN AD AGE');
						this.toBeSigned.waivers[index].guests[y].isMinor = (
							this.toBeSigned.waivers[index].guests[y].age < this.toBeSigned.waivers[index].currentWaiver.minAdultAge);
						// testing only.  this.toBeSigned.waivers[index].guests[y].isMinor = true;
						if (this.toBeSigned.waivers[index].guests[y].isMinor) {
							this.toBeSigned.waivers[index].guests[y].showGuardian = true;
							this.toBeSigned.waivers[index].guests[y].showInitialed = false;
						}
					}
				}

					this.addInlineInitials(this.currentWaiver.body);

				// tslint:disable-next-line:max-line-length
				if (this.toBeSigned.waivers[index].currentWaiver.electronicSignatureConsent === null || this.toBeSigned.waivers[index].currentWaiver.electronicSignatureConsent === '') {
					// tslint:disable-next-line:max-line-length
					this.toBeSigned.waivers[index].currentWaiver.electronicSignatureConsent = 'By checking here, you are consenting to the use of your electronic signature in lieu of an ' +
						'original signature on paper. You have the right to request that you sign a paper copy ' +
						'instead. By checking here, you are waiving that right. After consent, you may, upon written ' +
						'request to us, obtain a paper copy of an electronic record. No fee will be charged for such ' +
						'copy and no special hardware or software is required to view it. Your agreement to use an ' +
						'electronic signature with us for any documents will continue until such time as you notify ' +
						'us in writing that you no longer wish to use an electronic signature. There is no penalty ' +
						'for withdrawing your consent. You should always make sure that we have a current email ' +
						'address in order to contact you regarding any changes, if necessary.';
				}

				if(this.currentWaiver.showStop){
					this.showStop(this.currentWaiver.stopMessage);
				}
			});
	}

	addInlineInitials(waiverBody) {

		const splitWaiver = waiverBody.split(/\[\[(.*)\]\]/g);

		if (splitWaiver.length > 1) {
			this.hasInlineInitials = true;
			for (let i = 1; i < splitWaiver.length - 1; i = i + 2) {
				this.currentGuest.inlineInitials[i] = null;
			}
		}

		this.sectionedWaiver = splitWaiver;
	}

	async getInlineInitial(id) {
		const modal = await this.modalController.create({
			component: SignaturePage,
			componentProps: { title: this.initialsTitle, sig: this.currentGuest.inlineInitials[id] },
			backdropDismiss: false
		});

		modal.onDidDismiss().then((data: any) => {
			if (data.data.result === 'UPDATED') {
				this.currentGuest.inlineInitials[id] = data.data.sig;
				console.log(this.currentGuest.inlineInitials);
				if (!this.currentGuest.showConsent && this.allInitialsCompleted()) {
					this.currentGuest.showConsent = true;
					this.statusTabs.setCurrentTabNumber(1);
					const that = this;
					setTimeout(() => { that.scrollTo('#consent-card'); }, 150);
				}
			}
		});
		return await modal.present();
	}

	async getInitials() {

		const modal = await this.modalController.create({
			component: SignaturePage,
			componentProps: { title: this.initialsTitle, sig: this.currentGuest.initials },
			backdropDismiss: false
		});

		modal.onDidDismiss().then((data: any) => {
			if (data.data.result === 'UPDATED') {
				this.currentGuest.initials = data.data.sig;
				if (!this.currentGuest.showConsent) {
					this.currentGuest.initialed = true;
					if (this.allInitialsCompleted()) {
						this.currentGuest.showConsent = true;
						this.statusTabs.setCurrentTabNumber(1);
						const that = this;
						setTimeout(() => { that.scrollTo('#consent-card'); }, 150);
					}
				}
			}
		});
		return await modal.present();
	}

	allInitialsCompleted() {
		for (const initialId in this.currentGuest.inlineInitials) {
			if (!this.currentGuest.inlineInitials[initialId]) {
				return false;
			}
		}
		return this.currentGuest.initialed;
	}

	clickedConsent() {
		this.currentGuest.agreeToTerms = !this.currentGuest.agreeToTerms;
		if (this.currentGuest.agreeToTerms) {
			this.currentGuest.consent = true;
			this.currentGuest.showSigned = true;
			this.statusTabs.setCurrentTabNumber(2);
			const that = this;
			setTimeout(() => { that.scrollTo('#sign-card'); }, 150);
		} else {
			this.currentGuest.signature = null;
			this.currentGuest.showSubmit = false;
			this.currentGuest.showSigned = false;
			this.currentGuest.showInitialed = true;
			this.currentGuest.signed = false;
			this.statusTabs.setCurrentTabNumber(1);
		}
	}

	scrollTo(elementId: string) {
		const element: HTMLElement = document.querySelector(elementId);
		if (element) {
			const y = element.offsetTop;
			this.content.scrollToPoint(0, y, 500);
		}
	}

	async getSignature() {
		const sig = await this.modalController.create({
			component: SignaturePage,
			componentProps: { title: this.signatureTitle, sig: this.currentGuest.signature },
			backdropDismiss: false
		});

		sig.onDidDismiss().then((data: any) => {
			if (data.data.result === 'UPDATED') {
				this.currentGuest.signature = data.data.sig;
				this.currentGuest.signed = true;
				this.currentGuest.showInitialed = true;
				this.currentGuest.showConsent = true;
				this.currentGuest.showSigned = true;
				this.currentGuest.showSubmit = true;
				this.statusTabs.setCurrentTabNumber(3);
				const that = this;
				setTimeout(() => { that.scrollTo('#submit-card'); }, 150);
			}
		});

		return await sig.present();
	}

	guardianAgeCheck() {
		console.log(this.currentGuest.guardianDob);

		const today = moment();
		let dob = null;
		if(this.language.toLowerCase() == "en" || this.language.toLowerCase() == "en-us"){
			dob = moment(this.currentGuest.guardianDob);
		}else{
			dob = moment(this.currentGuest.guardianDob, "DD/MM/YYYY")
		}
		console.log(dob);
		let age = today.year() - dob.year();
		const m = today.month() - dob.month();
		if (m < 0 || (m === 0 && today.date() < dob.date())) {
			age--;
		}
		console.log(age);
		this.currentGuest.guardianAge = age;
		if (age) {
			this.currentGuest.guardianIsMinor = (age < this.currentWaiver.minAdultAge);
		}
		this.validateGuardian();
	}

	validateGuardian() {
		if (this.currentGuest.guardianAge && !this.currentGuest.guardianIsMinor) {
			if (this.currentGuest.guardianFirstName && this.currentGuest.guardianLastName && this.currentGuest.guardianEmail) {
				this.currentGuest.guardianFormIncomplete = false;
				this.currentGuest.showInitialed = true;
				const that = this;
				setTimeout(() => { that.scrollTo('#waiver-card'); }, 150);
			}
		}
		if (!(this.currentGuest.guardianFirstName && this.currentGuest.guardianLastName &&
			this.currentGuest.guardianEmail && this.currentGuest.guardianAge)) {
			this.currentGuest.guardianFormIncomplete = true;
		}
	}

	redoGuardian() {

		if(this.currentWaiver.body){
			this.currentGuest.signed = false;
			this.currentGuest.showInitialed = false;
			this.currentGuest.showConsent = false;
			this.currentGuest.showSigned = false;
			this.currentGuest.showSubmit = false;
			this.currentGuest.consent = false;
			this.currentGuest.agreeToTerms = false;
			this.currentGuest.signature = null;
			this.currentGuest.initials = null;
			this.addInlineInitials(this.currentWaiver.body);
			this.statusTabs.setCurrentTabNumber(0);
		}
	}

	randomBarcode(length) {
		var result           = '';
		var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for ( var i = 0; i < length; i++ ) {
		  result += characters.charAt(Math.floor(Math.random() * 
	 charactersLength));
	   }
	   return result;
	}

	async submitWaiver() {
		const now = new Date();
		
		this.signedWaiver.minors = [];
		this.signedWaiver.embeddedInitials = [];
		this.signedWaiver.documentId = this.currentWaiver.id;
		this.signedWaiver.active = true;
		this.signedWaiver.signSource = '1waiver-sommet';
		this.signedWaiver.signedDate = now.getTime();
		this.signedWaiver.signature = this.currentGuest.signature.substr(22);
		this.signedWaiver.email = this.currentGuest.email;
		this.signedWaiver.customData = [{ f1: '', f2: '', f3: '', f4: '', f5: '', f6: '', f7: '', f8: '', f9: '', f10: '' }];
		this.signedWaiver.barcode = this.randomBarcode(15);

		if (this.currentGuest.isMinor) {
			this.signedWaiver.guardianFirstName = this.currentGuest.guardianFirstName.trim();
			this.signedWaiver.guardianLastName = this.currentGuest.guardianLastName.trim();
			this.signedWaiver.guardianName = this.signedWaiver.guardianFirstName + ' ' + this.signedWaiver.guardianLastName;
			this.signedWaiver.guardianEmail = this.currentGuest.guardianEmail;
			if(this.language == "en" || this.language == "en-us"){
				this.signedWaiver.guardianDob  = moment(this.currentGuest.guardianDob).format();
			}else{
				this.signedWaiver.guardianDob = moment(this.currentGuest.guardianDob, "DD/MM/YYYY").format();
			}
			this.signedWaiver.age = this.currentGuest.guardianAge;
			const minor: any = {};
			minor.firstName = this.currentGuest.firstName;
			minor.lastName = this.currentGuest.lastName;
			minor.dateOfBirth = moment(this.currentGuest.dob).format();
			minor.age = this.currentGuest.age;
			minor.gender = null;
			minor.posGuestId = this.currentGuest.guestId;
			minor.posFormId = this.posWaiverId;
			if (this.currentGuest.address) {
				minor.addressLine1 = this.currentGuest.address.street1;
				minor.addressLine2 = this.currentGuest.address.street2;
				if (this.currentGuest.address.city && this.currentGuest.address.state){
					minor.city = this.currentGuest.address.city + ', ' + this.currentGuest.address.state;
				}else{
					minor.city = null;
				}
				minor.zip = this.currentGuest.address.postal;
			}
			if (minor.addressLine1 == null) {
				minor.addressLine1 = '';
			}
			if (minor.addressLine2 == null) {
				minor.addressLine2 = '';
			}
			minor.state = '';
			minor.phoneNumber = this.currentGuest.phone;
			this.signedWaiver.minors.push(minor);
		} else {
			if (this.currentGuest.address) {
				this.signedWaiver.address = this.currentGuest.address.street1;
				this.signedWaiver.address2 = this.currentGuest.address.street2;
				if (this.currentGuest.address.city && this.currentGuest.address.state){
					this.signedWaiver.city = this.currentGuest.address.city + ', ' + this.currentGuest.address.state;
				}else{
					this.signedWaiver.city = null;
				}
				this.signedWaiver.zip = this.currentGuest.address.postal;
			}
			if (this.signedWaiver.address == null) {
				this.signedWaiver.address = '';
			}
			if (this.signedWaiver.address2 == null) {
				this.signedWaiver.address2 = '';
			}
			this.signedWaiver.state = '';
			this.signedWaiver.phone = this.currentGuest.phone;
			this.signedWaiver.name = this.currentGuest.firstName + ' ' + this.currentGuest.lastName;
			this.signedWaiver.firstName = this.currentGuest.firstName;
			this.signedWaiver.lastName = this.currentGuest.lastName;
			this.signedWaiver.dateOfBirth = moment(this.currentGuest.dob).format();
			this.signedWaiver.age = this.currentGuest.age;
			this.signedWaiver.gender = null;
			this.signedWaiver.posGuestId = this.currentGuest.guestId;
			this.signedWaiver.posFormId = this.posWaiverId;
		}
		this.signedWaiver.embeddedInitials.push({ initialsId: 'ack', initials: this.currentGuest.initials.substr(22) });
		if (this.hasInlineInitials) {
			for (const initialId of Object.keys(this.currentGuest.inlineInitials)) {
				this.signedWaiver.embeddedInitials.push({ initialsId: initialId, initials: this.currentGuest.inlineInitials[initialId].substr(22) });
			}
		}

		console.log('signedWaiver', this.signedWaiver);
		console.log('selectedWaiver.config', this.selectedWaiver.config);
		const loading = await this.loadingCtrl.create({ message: 'Saving...', duration: 5000 });
		loading.present();
		this.dataService.submitWaiver(this.selectedWaiver.config, this.signedWaiver)
			.subscribe((data: any) => {
				if (data.success) {
					if (this.currentWaiver.emailGuest) {
						const emailPayload = {
							waiverDef: this.currentWaiver,
							signedWaiver: this.signedWaiver,
							logo: this.logo
						}
						console.log('emailPayload', emailPayload);
						this.emailService.sendEmail(emailPayload);
					}

					if (this.currentWaiver.printBarcode) {
						const emailPayload = {
							waiverDef: this.currentWaiver,
							signedWaiver: this.signedWaiver,
							logo: this.logo
						}
						console.log('emailPayload', emailPayload);
						this.emailService.sendBarcode(emailPayload);
					}

					this.currentGuest.submitted = true;
					this.statusTabs.tabsFinished();
					this.dataService.backupSave(data.data, this.toBeSigned.waivers[0].config.apiName, {});
					const that = this;
					setTimeout(() => { that.scrollTo('#submitted-card'); }, 150);


					if (this.pos.toUpperCase() === 'SW') {
						console.log(this.currentGuest.guestId, this.posWaiverId )
						this.dataService.acceptSWLiability(this.swURL, { guestNo: this.currentGuest.guestId, formNo: this.posWaiverId })
							.subscribe((data: any) => {
								console.log('SWAccept: ', data);
								this.currentGuest.submitted = true;
								this.currentGuest.showSubmit = false;
								this.currentGuest.showGuardian = false;
								this.currentGuest.showInitialed = false;
								this.currentGuest.showConsent = false;
								this.currentGuest.showSigned = false;
								this.currentGuest.showMoreToComplete = this.isProcessComplete();
								if (!this.currentGuest.showMoreToComplete) {
									console.log('postMessage: sw waiverCompleted');
									window.parent.postMessage(JSON.stringify({ messageType: 'waiverCompleted' }), '*');
								}
								loading.dismiss();
							}, err => {
								console.log(err);
								console.log('SWAccept: ', data);
								this.currentGuest.submitted = true;
								this.currentGuest.showSubmit = false;
								this.currentGuest.showGuardian = false;
								this.currentGuest.showInitialed = false;
								this.currentGuest.showConsent = false;
								this.currentGuest.showSigned = false;
								this.currentGuest.showMoreToComplete = this.isProcessComplete();
							});

					} else {

						this.currentGuest.showSubmit = false;
						this.currentGuest.showSigned = false;
						this.currentGuest.showInitialed = false;
						this.currentGuest.showConsent = false;
						this.currentGuest.showGuardian = false;
						this.currentGuest.showMoreToComplete = this.isProcessComplete();

						if (!this.currentGuest.showMoreToComplete) {
							console.log('postMessage: rtp waiverCompleted');
							window.parent.postMessage(JSON.stringify({ messageType: 'waiverCompleted' }), '*');
						}
						loading.dismiss();

					}



/*
					this.currentGuest.showSubmit = false;
					this.currentGuest.showSigned = false;
					this.currentGuest.showInitialed = false;
					this.currentGuest.showConsent = false;
					this.currentGuest.showGuardian = false;
					this.currentGuest.showMoreToComplete = this.isProcessComplete();
					if (!this.currentGuest.showMoreToComplete) {
						console.log('sending postMessage')
						window.parent.postMessage(JSON.stringify({ messageType: 'waiverCompleted' }), '*');
					}
					loading.dismiss();
*/
					/*
					this.dataService.acceptSWLiability(this.swURL, { guestNo: this.currentGuest.guestId, formNo: this.selectedWaiver.formId })
						.subscribe((data: any) => {
							console.log('SWAccept: ', data);
							this.currentGuest.submitted = true;
							this.currentGuest.showSubmit = false;
							this.currentGuest.showGuardian = false;
							this.currentGuest.showInitialed = false;
							this.currentGuest.showConsent = false;
							this.currentGuest.showSigned = false;
							this.currentGuest.showMoreToComplete = this.isProcessComplete();
							if (!this.currentGuest.showMoreToComplete) {
								window.parent.postMessage(JSON.stringify({ messageType: 'oneRiskClosed' }), '*');
							}
							loading.dismiss();
						}, err => {
							console.log(err);
							console.log('SWAccept: ', data);
							this.currentGuest.submitted = true;
							this.currentGuest.showSubmit = false;
							this.currentGuest.showGuardian = false;
							this.currentGuest.showInitialed = false;
							this.currentGuest.showConsent = false;
							this.currentGuest.showSigned = false;
							this.currentGuest.showMoreToComplete = this.isProcessComplete();
						});
						*/
				} else {
					loading.dismiss();
				}
			});
	}

	isProcessComplete() {
		let moreToSign = false;
		console.log('isProcessComplete', this.toBeSigned)
		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < this.toBeSigned.waivers.length; i++) {
			// tslint:disable-next-line:prefer-for-of
			for (let y = 0; y < this.toBeSigned.waivers[i].guests.length; y++) {
				if (!this.toBeSigned.waivers[i].guests[y].submitted) {
					moreToSign = true;
					break;
				}
			}
		}
		return moreToSign;
	}

}
