import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StatusTabsComponent } from './status-tabs.component';

describe('StatusTabsComponent', () => {
  let component: StatusTabsComponent;
  let fixture: ComponentFixture<StatusTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatusTabsComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StatusTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
