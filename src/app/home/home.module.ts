import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { StatusTabsComponent } from './status-tabs/status-tabs.component';

import { NgxMaskModule } from 'ngx-mask';
import { HomePageRoutingModule } from './home-routing.module';
import { SafePipe } from '../safe.pipe';
import { SignaturePage } from '../signature/signature.page';
import { SignaturePadModule } from 'angular2-signaturepad';
import { TranslateModule } from '@ngx-translate/core';
import { OneriskDatetimeComponent } from '../components/onerisk-datetime/onerisk-datetime.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
		HomePageRoutingModule,
		SignaturePadModule,
		TranslateModule,
		NgxMaskModule
  ],
	declarations: [HomePage, SafePipe, SignaturePage, StatusTabsComponent, OneriskDatetimeComponent],
	providers: [SafePipe],
	exports: [SafePipe, SignaturePadModule],
	entryComponents: [SignaturePage]
})

export class HomePageModule {}